import React from "react"
import {ReactComponent as FolderIcon} from "./folder.svg"

export default {
    folder_icon:<FolderIcon/>
}
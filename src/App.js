import React, {useState} from "react"
import {Button, Heading, MainContainer, RelativeBox, Widget} from "./compoents/styled";
import DropZone from "./compoents/DropZone";
import Loader from "./compoents/Loader";

function App() {
    const [loading, setLoading] = useState(false);
    return (
        <Widget>
            <MainContainer>
                <Heading>Upload a file with project costs</Heading>
                <RelativeBox>
                    <DropZone onUpload={() => setLoading(true)}/>
                    <Loader isLoading={loading} onCancel={() => setLoading(false)}/>
                </RelativeBox>
            </MainContainer>
            <Button disabled={loading} onClick={() => setLoading(true)} bordered centered>Add costs manually</Button>
        </Widget>
    );
}


export default App;

import styled from "styled-components";

export const colors = {
    white:"#ffffff",
    green: "#0CC599",
    black: "#232324",
    darkBlack: "#222222",
    pageBg: "#F8F8F8",
    dropZoneBg: "#F8FEFC",
    lightBlue: "#EFFAF7",
    lightBlueHover: "#E9F7F3",
    disabledGrey: "#AAAEB3",
    disabledBgGrey: "#F9F9F9",
    lightGrey: "#e8e8e8"
}

export const Button = styled.button`
  padding: 8px 16px;
  font-size: 14px;
  font-weight: 500;
  ${({disabled, bordered}) => disabled ? `
    background-color: ${colors.disabledBgGrey};
    color: ${colors.disabledGrey};
    border: ${bordered ? `1px solid ${colors.disabledGrey}` : 'none'};
  ` : `
    background-color: ${colors.lightBlue};
    color: ${colors.green};
    border: ${bordered ? `1px solid ${colors.green}` : 'none'};
     &:active {
    box-shadow: inset 0 0 4px ${colors.green};
  };
  &:hover {
    background-color: ${colors.lightBlueHover};
  };
  cursor: pointer;
  `};
  ${({centered}) => centered && `
    display: block;
    margin: 0 auto;
  `};
  outline: none;
  border-radius: 8px;
  transition: .1s ease;
  @media(max-width: 480px){
    font-size: 12px;
  }
`

export const Heading = styled.h3`
  color: ${colors.darkBlack};
  margin-bottom: 24px;
  margin-top: 0;
  text-align: center;
  @media(max-width: 480px){
    font-size: 16px;
  }
`

export const LoadingMessage = styled.h4`
  color: ${colors.darkBlack};
  margin-top: 0;
  text-align: center;
  font-family: Poppins,serif;
  @media(max-width: 480px){
    font-size: 14px;
  }
`

export const Widget = styled.div`
  max-width: 480px;
`

export const SpinnerContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;

  & svg {
    @keyframes spin {
      from {
        transform: rotate(0)
      }
      to {
        transform: rotate(360deg)
      }
    }
    animation-name: spin;
    animation-duration: 1s;
    animation-iteration-count: infinite;
    animation-timing-function: linear;
  }
`
export const RelativeBox = styled.div`
  position: relative;
`
export const DropZoneContainer = styled.div`
  background: ${colors.dropZoneBg};
  border: 1px dashed ${colors.green};
  box-sizing: border-box;
  border-radius: 12px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  color: ${colors.green};
  padding: 29px 24px;

  & span {
    font-size: 14px;
    font-weight: 500;
    margin-top: 13px;
    @media(max-width: 480px){
      font-size: 12px;
      text-align: center;
    }
  }
;
`

export const MainContainer = styled.div`
  background-color: ${colors.white};
  border: 1px solid #EFEFEF;
  box-sizing: border-box;
  box-shadow: 0px 2px 16px rgba(0, 0, 0, 0.04);
  border-radius: 16px;
  margin: 0 auto 64px;
  padding: 40px 80px 32px;
  @media(max-width: 480px){
    padding: 20px 40px 16px;
  };
  font-family: Poppins;
`

export const LoaderContainer = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background-color: ${colors.white};
  &>div{
    margin-bottom: 60px;
  }
  transition: .4s ease;
  position: absolute;
  top: 0;
  ${({isLoading}) => isLoading ? `
    opacity:1;
    pointer-events:all;
    padding-top:15px
  `: `
    opacity:0;
    pointer-events:none;
    padding-top: 0;
  `};
  &>button{
    background-color: transparent;
    border: none;
    outline: none;
    cursor: pointer;
    transition: .2s ease;
    display: block;
    margin: auto;
    font-weight: 600;
    font-size: 14px;
    line-height: 20px;
    &:hover{
      opacity: .6;
    }
  }
`


export const OrContainer = styled.div`
  text-align: center;
  color:${colors.black};
  opacity: 0.75;
  font-size: 12px;
  line-height: 20px;
  font-weight: 500;
  margin: 10px 0;
  
`


export const SvgContainer = styled.div`
  & svg {
    width: ${({width}) => width ?? '100%'};
    height: ${({height}) => height ?? '100%'};
  }
;
`
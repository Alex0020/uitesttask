import React from "react"
import {SpinnerContainer} from "../styled";
import {ReactComponent as SpinnerIcon} from "../../assets/svg/spinner_2.svg";


const Spinner = () => {
    return(
        <SpinnerContainer>
            <SpinnerIcon/>
        </SpinnerContainer>
    )
}

export default Spinner
import React from "react"
import assets from "../../assets/svg"
import {SvgContainer} from "../styled";


const Svg = ({size,name}) => {
    const svg = assets[name];
    return (
        <SvgContainer width={size?.width} height={size?.height}>
            {svg}
        </SvgContainer>
    );
}

export default Svg

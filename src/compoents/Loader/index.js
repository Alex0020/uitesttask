import React from "react"
import {LoaderContainer, LoadingMessage, SpinnerContainer} from "../styled";
import {ReactComponent as Spinner} from "../../assets/svg/spinner_2.svg";



const Loader = ({isLoading,onCancel}) => {

    return(
        <LoaderContainer isLoading={isLoading}>
            <div>
                <SpinnerContainer>
                    <Spinner/>
                </SpinnerContainer>
                <LoadingMessage>
                    Loading your file...
                </LoadingMessage>
            </div>
            <button onClick={onCancel}>Cancel</button>
        </LoaderContainer>
    )
}
export default Loader
import React from "react"
import {Button, DropZoneContainer, OrContainer} from "../styled";
import Svg from "../Svg";


const DropZone = ({onUpload}) => {
    return (
        <>
            <DropZoneContainer>
                <Svg name={"folder_icon"}/>
                <span>
                Drag and drop your file here
            </span>
            </DropZoneContainer>
            <OrContainer>
                - or -
            </OrContainer>
            <Button centered onClick={onUpload}>Browse</Button>
        </>
    )
}

export default DropZone
